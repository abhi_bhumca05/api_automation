import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Response2 = WS.sendRequest(findTestObject('SOAP/Currency_var', [('countryCD') : countryCode]))

WS.verifyElementText(Response2, 'CountryCurrencyResponse.CountryCurrencyResult.sName', RespCurr)

WS.verifyElementText(Response2, 'CountryCurrencyResponse.CountryCurrencyResult.sISOCode', RespCD)

println(GlobalVariable.Env_Detail)
System.out.println(GlobalVariable.Env_Detail)


if (countryCode == 'US') {
    println('ABhishek connected with Jenkins and first push4')

    WS.sendRequest(findTestObject('REST/POST'))
}
else 
{println'ABhishek connected with Jenkins and first push5'}

